# Text to speech bookmarklet

To use this simply create a bookmarklet using the following code:

    (function(){var script=document.createElement('script');script.setAttribute('src','https://gl.githack.com/jamiekp/text-to-speech-bookmarklet/-/raw/main/dist/tts.js');script.onload=function(){tts.textToSpeech('161509d9ec7949cebb910ae5d693d567',tts.getRandomVoice())};document.body.appendChild(script)})();