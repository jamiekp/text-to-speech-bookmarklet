const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    bundle: [
      './lib/microsoft.cognitiveservices.speech.sdk.bundle.js',
      './text-to-speech.js',
    ],
  },
  output: {
    filename: 'tts.js',
    path: path.resolve(__dirname, 'dist'),
    library: {
      name: 'tts',
      type: 'window'
    }
  },
};
